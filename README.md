# Celebration Notifier

# EN:
The Academix Team (http://academixproject.com/) wishes to celebrate the things that make us proud to be humans.  
For this reason we are launching the project “Celebration Notifier”, free and open for all.


The project's mission is to encourage those that contribute to creating a better society for us and for our children. 


Because alone we cannot find out about all the wonderful actions and the people that organize them, we ask you to let us know about them.  
(Political and fund raising actions are not in scope) 


The proposals will be selected by the Academix team and included in the project “Celebration Notifier” that is available for free on gitlab  
(https://gitlab.com/VBota1/celebration-notifier).  
Also the selected proposals will be included in the Academix distribution, where on each day of the year one of the selected activities will be shown.


The information added to the project will be distributed under MIT license
(https://gitlab.com/VBota1/celebration-notifier/blob/master/LICENSE).  
Please do not propose information that is not public or can not be distributed in a legal manner


**Useful links:**
* Academix on facebook: https://www.facebook.com/groups/academixgnulinux/
* “Celebration Notifier” project page: https://gitlab.com/VBota1/celebration-notifier

# RO:
Echipa Academix (http://academixproject.com/) dorește să serbeze lucrurile care ne fac mândrii că suntem oameni.  
De aceea lansăm proiectul “Celebration Notifier” gratuit și liber spre a fi utilizat de toți cei interesați.


Proiectul își propune încurajarea celor care contribuie la creerea unei societăți mai bune pentru noi și pentru copii noștri. 


Pentru că nu putem afla singuri despre toate acțiunile minunate și oamenii care le organizează, vă rugăm să ne spuneți despre și nouă despre ele.  
(Acțiunile politice și/sau colectarea de fonduri nu este în scop)


Propunerile vor fi selectate de echipa Academix și incluse în proiectul “Celebration Notifier”, disponibil gratuit pe gitlab 
(https://gitlab.com/VBota1/celebration-notifier).  
De asemenea propunerile selectate vor fi incluse în distribuția de linux Academix, unde în fiecare zi din an una din activitățile și/sau proiectele selectate va fi afișată.


Informațiile adăugate proiectului vor fi distribuite sub licență MIT (https://gitlab.com/VBota1/celebration-notifier/blob/master/LICENSE).  
Vă rugăm nu propuneți informații care nu sunt publice sau nu pot fi distribuite în mod legal.


**Legături utile:**
* Pagina de facebook a Academix: https://www.facebook.com/groups/academixgnulinux/
* Pagina de proiect “Celebration Notifier”: https://gitlab.com/VBota1/celebration-notifier 

# The Built application can be taken from here:
https://gitlab.com/VBota1/celebration-notifier/tree/master/selector/qt
