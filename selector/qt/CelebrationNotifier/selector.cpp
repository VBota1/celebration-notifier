/*
MIT License

Copyright (c) 2018-2019 Academix Team (contact@academixproject.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Academix Team members:
Luta Dumitru - Project Lead
Cristi Rusu – Testing and translation
Stefan Keresztes – Testing and translation
Sima Elena Madalina – Testing and translation
Stan Valentin – Testing and translation
Abibula Aygun – Public realtions, testing and developement
Caius Gherle – Sponsor, web and testing
Giovanni Brancato – Testing and translation
Nicola Grilli - Graphics
Georgeta Poenaru – Testing and translation
Dicu Florin Alexandru – Testing and translation
Adrian Nicolae Stan - Public relations and testing
Anca Rădoi - testing
Viorel Bota - Testing and developement
Victor Ciornea - Testing
Silviu Petrache -Testing
David Raul Dobai -Testing
Valentin Stroe -Testing and translation
Mihai-Stanislav Jalobeanu - Mentor, Doctor in Mathematics,
                            Professor in informatics, Researcher

3rd Party Licenses:
This work is dynamically linked to the Qt libraries installed on the target device.
The Qt Libraries are covered by the following LGPL3 that can be viewed here:
https://gitlab.com/VBota1/celebration-notifier/tree/master/3rdPartyLicenses

The Qt Libraryes includes multiple modules licensed under diferent conditions.
The link below contains a list of documents required by these licenses:
https://gitlab.com/VBota1/celebration-notifier/tree/master/3rdPartyLicenses

The Qt Libraryes used in this software use components based in part on the work
of the Independent JPEG Group.

The Qt Libraryes used in this software use components based in part on the work
of the PNG Reference Library

The Qt Libraryes used in this software use components that are
copyright © 1996-2002, 2006; The FreeType Project (www.freetype.org).

The Qt Libraryes used in this software use components based in part on the work
that is copyright (C) 1994-1996, Thomas G. Lane. (see ffmpeg_License.txt)

The text of this license and the 3rd Party Licenses is also shipped
with the binary build of this application.
 */

#include "selector.h"

Selector::Selector()
{
    Logger log;
    qInstallMessageHandler(log.logMessage);

    if (!isGlobalInhibitorActive())
        clearInhibitorsForAllExceptToday();
}

void Selector::clearInhibitorsForAllExceptToday() {
    QDate endDate = QDate(2020,12,31);
    QString folderToSkip = getFolderNameFromDate(QDate().currentDate());
    for (QDate day = QDate(2020,01,01); day<=endDate; day=day.addDays(1)) {
        QString folder = getFolderNameFromDate(day);
        if (folder.isNull())
            break;
        if (folder.isEmpty())
            continue;
        if (0==folder.compare(folderToSkip))
            continue;
        if (isCelebrationFromFolderPermited(folder))
            continue;
        QFile inhibitor (configFolder+folderDelimiter+inhibitorFileNameRoot+folder);
        inhibitor.remove();
    }
}

QString Selector::getTodaysSourceFolder() {
    if (todaysFolder.isNull())
        todaysFolder=getSourceFolderOfDay(QDate().currentDate());
    return todaysFolder;
}

QString Selector::getSourceFolderOfDay(QDate day) {
    if (!QDir(sourcesRoot).exists()) {
        qCritical(QLoggingCategory("Selector::getSourceFolderOfDay")) << "Missing resource parent folder " << sourcesRoot;
        return QString();
    }

    QString folder = sourcesRoot+folderDelimiter+
            getFolderNameFromDate(day)+folderDelimiter;
    if (QDir(folder).exists())
        return folder;

    qCritical(QLoggingCategory("Selector::getSourceFolderOfDay")) << "Missing resource folder " << folder;
    return QString("");
}

QString Selector::getFolderNameFromDate(QDate date) {
    return QString("M")+date.toString("MM")+
            QString("D")+date.toString("dd");
}

bool Selector::isCelebrationFromFolderPermited(QString folder) {
    QFileInfo check_file(QDir::homePath()+folderDelimiter+configFolder+folderDelimiter+inhibitorFileNameRoot+folder);
    return !(check_file.exists() && check_file.isFile());
}

void Selector::inhibitCelebrationInFolder(QString folder) {
    QString configFolderWithPath = QDir::homePath()+folderDelimiter+configFolder;
    if (!QDir(configFolderWithPath).exists())
        return;
    QFile inhibitor(configFolderWithPath+folderDelimiter+inhibitorFileNameRoot+folder);
    inhibitor.open(QIODevice::WriteOnly);
    inhibitor.close();
}

QUrl Selector::getUrlToShow()
{
    QString resourceFolder = getTodaysSourceFolder();

    if (isGlobalInhibitorActive())
        return QUrl();

    if (resourceFolder.isNull() || resourceFolder.isEmpty())
        return QUrl();

    QString srouceFolderForToday = getFolderNameFromDate(QDate().currentDate());
    if (!isCelebrationFromFolderPermited(srouceFolderForToday))
        return QUrl();

    inhibitCelebrationInFolder(srouceFolderForToday);

    QString resourceFile = getResourceFileFromFolder(resourceFolder);
    if (resourceFile.isNull() || resourceFile.isEmpty())
        return QUrl();

    return QUrl(resourceFile);
}

QString Selector::getResourceFileFromFolder(QString folder) {
    QString resourceFile = folder+infoPageFile;
    QFileInfo check_file(resourceFile);
    if (check_file.exists() && check_file.isFile())
        return "file:" + resourceFile;
    qWarning(QLoggingCategory("Selector::getResourceFileFromFolder")) << "Missing resource file " << resourceFile;
    return QString();
}

bool Selector::isGlobalInhibitorActive() {
    QFileInfo check_file(QDir::homePath()+folderDelimiter+configFolder+folderDelimiter+globalInhibitorName);
    return (check_file.exists() && check_file.isFile());
}

QUrl Selector::getUrlWithMoreInformation() {
    QString resourceFolder = getTodaysSourceFolder();
    if (resourceFolder.isNull() || resourceFolder.isEmpty())
        return QUrl();

    QString resourceFileName = resourceFolder+moreInfoFile;
    QFile resourceFile(resourceFileName);
    QFileInfo check_file(resourceFile);
    if (!check_file.exists() || !check_file.isFile()) {
        qWarning(QLoggingCategory("Selector::getUrlWithMoreInformation")) << "Missing resource file " << resourceFileName;
        return QUrl();
    }

    resourceFile.open(QFile::ReadOnly | QFile::Text);
    QUrl response(resourceFile.readAll().trimmed());
    resourceFile.close();

    return response;
}

QString Selector::getLicense() {
    QString licenseText;
    QFile license(resourceRoot+folderDelimiter+licenseFile);
    QFileInfo check_file(license);
    if (check_file.exists() && check_file.isFile()) {
        license.open(QFile::ReadOnly | QFile::Text);
        licenseText = QString(license.readAll().trimmed());
        license.close();
        licenseText = applicationName + " Version: " + version + " " + licenseText;
    }
    else
        qCritical(QLoggingCategory("Selector::getLicense")) << "Missing License file " << license.Text;

    return licenseText;
}

QIcon Selector::getLlogo() {
    if (logo.isNull()) {
        QFileInfo check_file(logoFile);
        if (check_file.exists() && check_file.isFile())
            logo = QIcon(logoFile);
        else
            qCritical(QLoggingCategory("Selector::getLlogo")) << "Missing logo file " << logoFile;
    }
    return logo;
}
